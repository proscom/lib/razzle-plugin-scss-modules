// Based on https://github.com/facebook/create-react-app/blob/master/packages/react-scripts/config/webpack.config.js

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const paths = require('razzle/config/paths');
const getLocalIdent = require('./getLocalIdent');

const defaultOptions = {
  postcss: {
    dev: {
      sourceMap: true,
      ident: 'postcss',
    },
    prod: {
      sourceMap: true,
      ident: 'postcss',
    },
    plugins: [
      require('postcss-flexbugs-fixes'),
      require('postcss-preset-env')({
        autoprefixer: {
          flexbox: 'no-2009',
        },
        stage: 3,
      }),
      require('postcss-normalize')()
    ],
  },
  sass: {
    dev: {
      sourceMap: true,
      includePaths: [paths.appNodeModules],
    },
    prod: {
      // XXX Source maps are required for the resolve-url-loader to properly
      // function. Disable them in later stages if you do not want source maps.
      sourceMap: true,
      sourceMapContents: false,
      includePaths: [paths.appNodeModules],
    },
  },
  css: {
    dev: {
      sourceMap: true,
      importLoaders: 1,
      modules: false,
    },
    prod: {
      sourceMap: true,
      importLoaders: 1,
      modules: false,
    },
  },
  style: {},
  resolveUrl: {
    dev: {},
    prod: {},
  },
  forceExtract: false
};

module.exports = (
  defaultConfig,
  { target, dev },
  webpack,
  userOptions = {}
) => {
  const isServer = target !== 'web';
  const constantEnv = dev ? 'dev' : 'prod';

  const config = Object.assign({}, defaultConfig);

  const options = Object.assign({}, defaultOptions, userOptions);

  const cssOptionsModules = {
    ...options.css[constantEnv],
    modules: {
      getLocalIdent,
    },
  };

  const styleLoader = {
    loader: require.resolve('style-loader'),
    options: options.style,
  };

  const cssLoader = {
    loader: require.resolve('css-loader'),
    options: options.css[constantEnv],
  };

  const cssLoaderModules = {
    loader: require.resolve('css-loader'),
    options: cssOptionsModules
  };

  const resolveUrlLoader = {
    loader: require.resolve('resolve-url-loader'),
    options: options.resolveUrl[constantEnv],
  };

  const postCssLoader = {
    loader: require.resolve('postcss-loader'),
    options: Object.assign({}, options.postcss[constantEnv], {
      plugins: () => options.postcss.plugins,
    }),
  };

  const sassLoader = {
    loader: require.resolve('sass-loader'),
    options: options.sass[constantEnv],
  };

  config.module.rules = config.module.rules.filter((rule) => {
    return !(rule.test && (
      rule.test.toString() === /\.css$/.toString() ||
      rule.test.toString() === /\.module\.css$/.toString()
    ));
  });

  config.module.rules = [
    ...config.module.rules,
    {
      test: /\.css$/,
      exclude: [paths.appBuild, /\.module\.css$/],
      use: isServer
        ? [
          {
            loader: require.resolve('css-loader'),
            options: {
              ...options.css[constantEnv],
              onlyLocals: true
            }
          },
          postCssLoader,
        ]
        : [
          dev && !options.forceExtract ? styleLoader : MiniCssExtractPlugin.loader,
          cssLoader,
          postCssLoader,
        ]
    },
    {
      test: /\.module\.css$/,
      exclude: [paths.appBuild],
      use: isServer
        ? [
          {
            loader: require.resolve('css-loader'),
            options: {
              ...cssOptionsModules,
              onlyLocals: true
            }
          },
          postCssLoader,
        ]
        : [
          dev && !options.forceExtract ? styleLoader : MiniCssExtractPlugin.loader,
          cssLoaderModules,
          postCssLoader,
        ]
    },
    {
      test: /\.module\.(sa|sc)ss$/,
      exclude: [paths.appBuild],
      use: isServer
        ? [
          {
            loader: require.resolve('css-loader'),
            options: {
              ...cssOptionsModules,
              onlyLocals: true
            },
          },
          resolveUrlLoader,
          postCssLoader,
          sassLoader,
        ]
        : [
          dev && !options.forceExtract ? styleLoader : MiniCssExtractPlugin.loader,
          cssLoaderModules,
          postCssLoader,
          resolveUrlLoader,
          sassLoader,
        ],
    },
    {
      test: /\.(sa|sc)ss$/,
      exclude: [paths.appBuild, /\.module\.(sa|sc)ss$/],
      use: isServer
        ? [
            {
              loader: require.resolve('css-loader'),
              options: {
                ...options.css[constantEnv],
                onlyLocals: true
              },
            },
            resolveUrlLoader,
            postCssLoader,
            sassLoader,
          ]
        : [
            dev && !options.forceExtract ? styleLoader : MiniCssExtractPlugin.loader,
            cssLoader,
            postCssLoader,
            resolveUrlLoader,
            sassLoader,
          ],
    },
  ];

  if (options.forceExtract && !isServer) {
    config.plugins.push(
      new MiniCssExtractPlugin({
        filename: 'static/css/bundle.[contenthash:8].css',
        chunkFilename: 'static/css/[name].[contenthash:8].chunk.css',
        // allChunks: true because we want all css to be included in the main
        // css bundle when doing code splitting to avoid FOUC:
        // https://github.com/facebook/create-react-app/issues/2415
        allChunks: true,
      }),
    );
  }

  return config;
};
