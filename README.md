# Razzle SCSS Modules Plugin

This plugins updates the default CSS configuration and adds support for SCSS
and SCSS modules.

It completely replaces [razzle-plugin-scss](https://www.npmjs.com/package/razzle-plugin-scss). 

## Installation

```
npm install --save-dev @proscom/razzle-plugin-scss-modules
// or
yarn add --dev @proscom/razzle-plugin-scss-modules
```

## Usage

```
// razzle.config.js

module.exports = {
    plugins: [
        { 
            func: require('@proscom/razzle-plugin-scss-modules'),
            options: {} 
        }
    ]
};

```

## Options

Options are the same as in [razzle-plugin-scss](https://www.npmjs.com/package/razzle-plugin-scss)
